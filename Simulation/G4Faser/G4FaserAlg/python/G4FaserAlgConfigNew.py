#
#  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
from AthenaCommon.AppMgr import *

#
#  Manager classes for detector geometry and sensitive detectors
#
from G4FaserServices.G4FaserServicesConfigNew import DetectorGeometrySvcCfg, FaserGeoIDSvcCfg
from G4FaserTools.G4FaserToolsConfig import getSensitiveDetectorMasterTool
#
#  Framework utilities
#
from ISF_Services.ISF_ServicesConfigNew import MC15aPlusTruthServiceCfg, InputConverterCfg
#
#  The GeoIDSvcCfg call is a disaster, bringing in tons of ATLAS-specific
#  stuff that would have to be rewritten; instead, we define FASER as being
#  unconditionally in the "undefined" ATLAS region; this may eventually limit
#  the granularity of truth strategies we can use
#
#from ISF_Services.ISF_ServicesConfigNew import GeoIDSvcCfg
#
from  G4AtlasAlg.G4AtlasAlgConf import G4AtlasAlg

#to still migrate: getAthenaStackingActionTool, getAthenaTrackingActionTool

def G4FaserAlgCfg(ConfigFlags, name='G4FaserAlg', **kwargs):
    #
    #  add Services to G4AtlasAlg
    #
    result = DetectorGeometrySvcCfg(ConfigFlags)
    kwargs.setdefault('DetGeoSvc', result.getService("DetectorGeometrySvc"))
    #
    #  MC particle container names
    #
    kwargs.setdefault("InputTruthCollection", "GEN_EVENT") 
    kwargs.setdefault("OutputTruthCollection", "TruthEvent")
    #
    #  Option to free memory by dropping GeoModel after construction
    #
    # if ConfigFlags.Sim.ReleaseGeoModel:
    kwargs.setdefault('ReleaseGeoModel', ConfigFlags.Sim.ReleaseGeoModel)
    #
    #  Record the particle flux during the simulation
    #
    # if ConfigFlags.Sim.RecordFlux:
    kwargs.setdefault('RecordFlux' , ConfigFlags.Sim.RecordFlux)
    #
    #  Treatment of bad events
    #
    # if ConfigFlags.Sim.FlagAbortedEvents:
        ## default false
    kwargs.setdefault('FlagAbortedEvents' ,ConfigFlags.Sim.FlagAbortedEvents)
    if ConfigFlags.Sim.FlagAbortedEvents and ConfigFlags.Sim.KillAbortedEvents:
            print('WARNING When G4AtlasAlg.FlagAbortedEvents is True G4AtlasAlg.KillAbortedEvents should be False!!! Setting G4AtlasAlg.KillAbortedEvents = False now!')
            kwargs.setdefault('KillAbortedEvents' ,False)
    # if  ConfigFlags.Sim.KillAbortedEvents:
        ## default true
    kwargs.setdefault('KillAbortedEvents' ,ConfigFlags.Sim.KillAbortedEvents)
    #
    #  Random numbers
    #
    from RngComps.RandomServices import AthEngines, RNG
    if ConfigFlags.Random.Engine in AthEngines.keys():
        result.merge(RNG(ConfigFlags.Random.Engine, name="AthRNGSvc"))
        kwargs.setdefault("AtRndmGenSvc",result.getService("AthRNGSvc"))

    kwargs.setdefault("RandomGenerator", "athena")
    #
    #  Multi-threading settinggs
    #
    is_hive = ConfigFlags.Concurrency.NumThreads > 1
    kwargs.setdefault('MultiThreading', is_hive)
    #
    #  What truth information to save?
    #
    accMCTruth = MC15aPlusTruthServiceCfg(ConfigFlags)
    result.merge(accMCTruth)
    kwargs.setdefault('TruthRecordService', result.getService("ISF_MC15aPlusTruthService"))
    #kwargs.setdefault('TruthRecordService', ConfigFlags.Sim.TruthStrategy) # TODO need to have manual override (simFlags.TruthStrategy.TruthServiceName())
    #
    #  Run-time geometry navigation for truth filtering - the ISF service is incompatible with FASER
    #
    # accGeoID = GeoIDSvcCfg(ConfigFlags)
    # result.merge(accGeoID)
    # kwargs.setdefault('GeoIDSvc', result.getService('ISF_GeoIDSvc'))
    accGeoID = FaserGeoIDSvcCfg(ConfigFlags)
    result.merge(accGeoID)
    kwargs.setdefault('GeoIDSvc', result.getService('ISF_FaserGeoIDSvc'))
    #
    #  Converts generator particles to a proprietary type managed by ISF
    #
    accInputConverter = InputConverterCfg(ConfigFlags)
    result.merge(accInputConverter)
    kwargs.setdefault('InputConverter', result.getService("ISF_InputConverter"))
    #
    #  Output level
    #
    ## G4AtlasAlg verbosities (available domains = Navigator, Propagator, Tracking, Stepping, Stacking, Event)
    ## Set stepper verbose = 1 if the Athena logging level is <= DEBUG
    # TODO: Why does it complain that G4AtlasAlgConf.G4AtlasAlg has no "Verbosities" object? Fix.
    verbosities=dict()
    #from AthenaCommon.AppMgr import ServiceMgr
    #if ServiceMgr.MessageSvc.OutputLevel <= 2:
    #    verbosities["Tracking"]='1'
    #    print verbosities
    kwargs.setdefault('Verbosities', verbosities)
    #
    # Set commands for the G4AtlasAlg
    #
    kwargs.setdefault("G4Commands", ConfigFlags.Sim.G4Commands)
    #
    # Default to using all known sensitive detectors
    #
    # result.addPublicTool(getSensitiveDetectorMasterTool())
    # kwargs.setdefault("SenDetMasterTool", result.getPublicTool("SensitiveDetectorMasterTool"))

    result.addEventAlgo(G4AtlasAlg(name, **kwargs))
    return result

if __name__ == '__main__':
  from AthenaConfiguration.MainServicesConfig import MainServicesSerialCfg
  import os

  # Set up logging and config behaviour
  from AthenaCommon.Logging import log
  from AthenaCommon.Constants import DEBUG
  from AthenaCommon.Configurable import Configurable
  log.setLevel(DEBUG)
  Configurable.configurableRun3Behavior = 1


  #import config flags
  from AthenaConfiguration.AllConfigFlags import ConfigFlags
  
  from AthenaConfiguration.TestDefaults import defaultTestFiles
  inputDir = defaultTestFiles.d
  ConfigFlags.Input.Files = defaultTestFiles.EVNT

  ConfigFlags.Sim.WorldRRange = 15000
  ConfigFlags.Sim.WorldZRange = 27000 #change defaults?
  ConfigFlags.Detector.SimulateForward = False
  # Finalize 
  ConfigFlags.lock()

  ## Initialize a new component accumulator
  cfg = MainServicesSerialCfg()

  #add the algorithm
  cfg.merge(G4FaserAlgCfg(ConfigFlags))

  # Dump config
  cfg.getService("StoreGateSvc").Dump = True
  cfg.getService("ConditionStore").Dump = True
  cfg.printConfig(withDetails=True, summariseProps = True)
  ConfigFlags.dump()


  # Execute and finish
  sc = cfg.run(maxEvents=3)
  # Success should be 0
  os.sys.exit(not sc.isSuccess())

  #f=open("test.pkl","w")
  #cfg.store(f) 
  #f.close()



  print(cfg._publicTools)
  print("-----------------finished----------------------")
