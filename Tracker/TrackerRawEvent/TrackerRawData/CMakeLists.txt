################################################################################
# Package: TrackerRawData
################################################################################

# Declare the package name:
atlas_subdir( TrackerRawData )

# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          Control/AthContainers
                          Control/AthLinks
                          Control/AthenaKernel
                          DetectorDescription/Identifier
                          Event/EventContainers
                          PRIVATE
                          GaudiKernel )

# External dependencies:
find_package( ROOT COMPONENTS Core Tree MathCore Hist RIO pthread )

# Component(s) in the package:
atlas_add_library( TrackerRawData
                   src/*.cxx
                   PUBLIC_HEADERS TrackerRawData
                   PRIVATE_INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                   LINK_LIBRARIES AthenaKernel AthContainers AthLinks Identifier EventContainers
                   PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES} GaudiKernel )

atlas_add_dictionary( TrackerRawDataDict
                      TrackerRawData/TrackerRawDataCLASS_DEF.h
                      TrackerRawData/selection.xml
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} AthContainers AthLinks Identifier GaudiKernel TrackerRawData EventContainers )

