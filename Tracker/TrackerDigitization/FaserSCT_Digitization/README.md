This package will translate Geant4 hits (HITS) into RawDataObject (RDO) data files.

Because input data and alignment is needed, there are a few steps to prepare. From a run directory where calypso is installed (displayed in the command-line prompt as `run >` below), do:

```
run > source ./setup.sh
```

This sets up the runtime environment after building. The "." is mandatory.

```
run > runG4.py >& runG4.log
```

This will create a HITS file.

```
run > python python/GeoModelTest/GeoModelTestConfig.py
```

This will generate a set of neutral alignment data

```
run > CopyAlignFolder.sh
```

This will merge the alignment data into the conditions DB.

All the above steps need only be done once after building calypso (but the last two steps must be repeated each time you do `make install`).

Now you can run digitization on the HITS file you generated with the command:

```
run > FaserSCT_DigitizationDbg.py >& digi.log
```

This will read your `g4.HITS.root` and generate an RDO data file with digitized raw data objects.

