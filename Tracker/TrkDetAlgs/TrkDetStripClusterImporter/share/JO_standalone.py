from AthenaCommon import CfgMgr
import AthenaRootComps.ReadAthenaRoot
svcMgr.EventSelector.InputCollections = ["/afs/cern.ch/work/r/rjansky/FASER/clusters.root"]
svcMgr.EventSelector.TupleName = "clusters"


# Access the algorithm sequence:
from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

from TrkDetStripClusterImporter.TrkDetStripClusterImporterConf import TrkDet__StripClusterImporter
# Add the algorithm.
importalg = TrkDet__StripClusterImporter("StripClusterImporter")
importalg.OutputLevel = VERBOSE
topSequence += importalg
print importalg

from OutputStreamAthenaPool.MultipleStreamManager import MSMgr
xaodStream = MSMgr.NewPoolRootStream( "StreamAOD", "XAOD_"+"clusters"+".pool.root" )
xaodStream.AddItem("xAOD::StripClusterContainer#*")
xaodStream.AddItem("xAOD::StripClusterAuxContainer#*")