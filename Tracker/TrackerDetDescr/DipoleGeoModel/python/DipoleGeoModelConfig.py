# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

from AthenaCommon import CfgMgr

def getDipoleTool(name="DipoleTool", **kwargs):
    kwargs.setdefault("RDBAccessSvc",     "RDBAccessSvc")
    kwargs.setdefault("GeometryDBSvc",    "TrackerGeometryDBSvc")
    kwargs.setdefault("GeoDbTagSvc",      "GeoDbTagSvc")
    return CfgMgr.DipoleTool(name, **kwargs)


def DipoleGeometryCfg( flags ):
    from FaserGeoModel.GeoModelConfig import GeoModelCfg
    acc = GeoModelCfg( flags )
    geoModelSvc=acc.getPrimary()
    
    from GeometryDBSvc.GeometryDBSvcConf import GeometryDBSvc
    acc.addService(GeometryDBSvc("TrackerGeometryDBSvc"))
    
    from RDBAccessSvc.RDBAccessSvcConf import RDBAccessSvc
    acc.addService(RDBAccessSvc("RDBAccessSvc"))

    from DBReplicaSvc.DBReplicaSvcConf import DBReplicaSvc
    acc.addService(DBReplicaSvc("DBReplicaSvc"))

    from DipoleGeoModel.DipoleGeoModelConf import DipoleTool
    dipoleTool = DipoleTool()

    geoModelSvc.DetectorTools += [ dipoleTool ]
    
    return acc
