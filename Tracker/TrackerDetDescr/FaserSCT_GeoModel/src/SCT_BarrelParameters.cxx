/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "FaserSCT_GeoModel/SCT_BarrelParameters.h"
#include "FaserSCT_GeoModel/SCT_GeometryManager.h"

#include "FaserSCT_GeoModel/SCT_DataBase.h"

#include "RDBAccessSvc/IRDBRecord.h"
#include "GaudiKernel/SystemOfUnits.h"

#include <cmath>


SCT_BarrelParameters::SCT_BarrelParameters(SCT_DataBase* rdb)
{
  m_rdb = rdb;
}


//
// Barrel Layer
//

  double
  SCT_BarrelParameters::etaHalfPitch() const
  {
    return m_rdb->brlGeneral()->getDouble("ETAHALFPITCH");
  }

  double
  SCT_BarrelParameters::phiHalfPitch() const
  {
    return m_rdb->brlGeneral()->getDouble("PHIHALFPITCH");
  }

  double
  SCT_BarrelParameters::depthHalfPitch() const
  {
    return m_rdb->brlGeneral()->getDouble("DEPTHHALFPITCH");
  }

  double
  SCT_BarrelParameters::sideHalfPitch() const
  {
    return m_rdb->brlGeneral()->getDouble("SIDEHALFPITCH");
  }

//
// Barrel General
//
int
SCT_BarrelParameters::numLayers() const
{
  return m_rdb->brlGeneral()->getInt("NUMLAYERS"); 
}

double
SCT_BarrelParameters::layerPitch() const
{
  return m_rdb->brlGeneral()->getDouble("LAYERPITCH");
}
