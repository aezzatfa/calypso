#!/bin/env python

sctChannels = ['1073741824 ', '1075838976 ', '1077936128 ', '1080033280 ', '1082130432 ', '1084227584 ', '1086324736 ', '1088421888 ', '1090519040 ', '1092616192 ', '1094713344 ', '1096810496 ', '1098907648 ', '1101004800 ', '1103101952 ', '1105199104 ', '1107296256 ', '1109393408 ', '1111490560 ', '1113587712 ', '1115684864 ', '1117782016 ', '1119879168 ', '1121976320 ', '1124073472 ', '1126170624 ', '1128267776 ', '1130364928 ', '1132462080 ', '1134559232 ', '1136656384 ', '1138753536 ', '1140850688 ', '1142947840 ', '1145044992 ', '1147142144 ', '1149239296 ', '1151336448 ', '1153433600 ', '1155530752 ', '1157627904 ', '1159725056 ', '1161822208 ', '1163919360 ', '1166016512 ', '1168113664 ', '1170210816 ', '1172307968 ', '1207959552 ', '1210056704 ', '1212153856 ', '1214251008 ', '1216348160 ', '1218445312 ', '1220542464 ', '1222639616 ', '1224736768 ', '1226833920 ', '1228931072 ', '1231028224 ', '1233125376 ', '1235222528 ', '1237319680 ', '1239416832 ', '1241513984 ', '1243611136 ', '1245708288 ', '1247805440 ', '1249902592 ', '1251999744 ', '1254096896 ', '1256194048 ', '1258291200 ', '1260388352 ', '1262485504 ', '1264582656 ', '1266679808 ', '1268776960 ', '1270874112 ', '1272971264 ', '1275068416 ', '1277165568 ', '1279262720 ', '1281359872 ', '1283457024 ', '1285554176 ', '1287651328 ', '1289748480 ', '1291845632 ', '1293942784 ', '1296039936 ', '1298137088 ', '1300234240 ', '1302331392 ', '1304428544 ', '1306525696 ', '1342177280 ', '1344274432 ', '1346371584 ', '1348468736 ', '1350565888 ', '1352663040 ', '1354760192 ', '1356857344 ', '1358954496 ', '1361051648 ', '1363148800 ', '1365245952 ', '1367343104 ', '1369440256 ', '1371537408 ', '1373634560 ', '1375731712 ', '1377828864 ', '1379926016 ', '1382023168 ', '1384120320 ', '1386217472 ', '1388314624 ', '1390411776 ', '1392508928 ', '1394606080 ', '1396703232 ', '1398800384 ', '1400897536 ', '1402994688 ', '1405091840 ', '1407188992 ', '1409286144 ', '1411383296 ', '1413480448 ', '1415577600 ', '1417674752 ', '1419771904 ', '1421869056 ', '1423966208 ', '1426063360 ', '1428160512 ', '1430257664 ', '1432354816 ', '1434451968 ', '1436549120 ', '1438646272 ', '1440743424 ']

description = '<timeStamp>run-lumi</timeStamp><addrHeader><address_header clid="1238547719" service_type="71" /></addrHeader><typeName>CondAttrListCollection</typeName>'

descriptionDCS = '<timeStamp>time</timeStamp><addrHeader><address_header service_type="71" clid="1238547719" /></addrHeader><typeName>CondAttrListCollection</typeName><cache>600</cache>'

# descriptionMagnet = '<timeStamp>time</timeStamp><addrHeader><address_header service_type="71" clid="1238547719" /></addrHeader><typeName>CondAttrListCollection</typeName><cache>600</cache><named/>'

from PyCool import cool, coral

dbSvc = cool.DatabaseSvcFactory.databaseService()
connectString = 'sqlite://;schema=ALLP200.db;dbname=OFLP200'

print 'recreating database'
dbSvc.dropDatabase( connectString )
db = dbSvc.createDatabase( connectString )

gainSpec = cool.RecordSpecification()
gainSpec.extend( 'serialNumber'          , cool.StorageType.UInt63 )
gainSpec.extend( 'runNumber'             , cool.StorageType.UInt32 )
gainSpec.extend( 'scanNumber'            , cool.StorageType.UInt32 )
gainSpec.extend( 'gainByChip'            , cool.StorageType.String4k )
gainSpec.extend( 'gainRMSByChip'         , cool.StorageType.String4k )
gainSpec.extend( 'offsetByChip'          , cool.StorageType.String4k )
gainSpec.extend( 'offsetRMSByChip'       , cool.StorageType.String4k )
gainSpec.extend( 'noiseByChip'           , cool.StorageType.String4k )
gainSpec.extend( 'noiseRMSByChip'        , cool.StorageType.String4k )

gainRecord = cool.Record(gainSpec)
gainRecord[ 'serialNumber'    ] = 0
gainRecord[ 'runNumber'       ] = 0
gainRecord[ 'scanNumber'      ] = 0
gainRecord[ 'gainByChip'      ] = '52.0 52.0 52.0 52.0 52.0 52.0 52.0 52.0 52.0 52.0 52.0 52.0'
gainRecord[ 'gainRMSByChip'   ] = '1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25'
gainRecord[ 'offsetByChip'    ] = '45.0 45.0 45.0 45.0 45.0 45.0 45.0 45.0 45.0 45.0 45.0 45.0'
gainRecord[ 'offsetRMSByChip' ] = '1.75 1.75 1.75 1.75 1.75 1.75 1.75 1.75 1.75 1.75 1.75 1.75'
gainRecord[ 'noiseByChip'     ] = '1600.0 1600.0 1600.0 1600.0 1600.0 1600.0 1600.0 1600.0 1600.0 1600.0 1600.0 1600.0'
gainRecord[ 'noiseRMSByChip'  ] = '45.0 45.0 45.0 45.0 45.0 45.0 45.0 45.0 45.0 45.0 45.0 45.0'

gainFolderSpec = cool.FolderSpecification(gainSpec)
gainFolder = db.createFolder('/SCT/DAQ/Calibration/ChipGain', gainFolderSpec, description, True)

for channel in sctChannels:
    gainFolder.storeObject( cool.ValidityKeyMin, cool.ValidityKeyMax, gainRecord, int(channel) )

noiseSpec = cool.RecordSpecification()
noiseSpec.extend( 'serialNumber'          , cool.StorageType.UInt63 )
noiseSpec.extend( 'runNumber'             , cool.StorageType.UInt32 )
noiseSpec.extend( 'scanNumber'            , cool.StorageType.UInt32 )
noiseSpec.extend( 'offsetByChip'          , cool.StorageType.String4k )
noiseSpec.extend( 'occupancyByChip'       , cool.StorageType.String4k )
noiseSpec.extend( 'occupancyRMSByChip'    , cool.StorageType.String4k )
noiseSpec.extend( 'noiseByChip'           , cool.StorageType.String4k )

noiseRecord = cool.Record(noiseSpec)
noiseRecord[ 'serialNumber'       ] = 0
noiseRecord[ 'runNumber'          ] = 0
noiseRecord[ 'scanNumber'         ] = 0
noiseRecord[ 'offsetByChip'       ] = '60.0 60.0 60.0 60.0 60.0 60.0 60.0 60.0 60.0 60.0 60.0 60.0'
noiseRecord[ 'occupancyByChip'    ] = '3.50e-05 3.50e-05 3.50e-05 3.50e-05 3.50e-05 3.50e-05 3.50e-05 3.50e-05 3.50e-05 3.50e-05 3.50e-05 3.50e-05 3.50e-05'
noiseRecord[ 'occupancyRMSByChip' ] = '2.50e-05 2.50e-05 2.50e-05 2.50e-05 2.50e-05 2.50e-05 2.50e-05 2.50e-05 2.50e-05 2.50e-05 2.50e-05 2.50e-05 2.50e-05'

noiseFolderSpec = cool.FolderSpecification(noiseSpec)
noiseFolder = db.createFolder('/SCT/DAQ/Calibration/ChipNoise', noiseFolderSpec, description, True)

for channel in sctChannels:
    noiseFolder.storeObject( cool.ValidityKeyMin, cool.ValidityKeyMax, noiseRecord, int(channel) )

# magnetSpec = cool.RecordSpecification()
# magnetSpec.extend( 'value' , cool.StorageType.Float )
# magnetSpec.extend( 'quality_invalid' , cool.StorageType.Bool )

# magnetRecord = cool.Record(magnetSpec)
# magnetRecord[ 'quality_invalid'] = 1

# magnetFolderSpec = cool.FolderSpecification(magnetSpec)
# magnetFolder = db.createFolder('/EXT/DCS/MAGNETS/SENSORDATA', magnetFolderSpec, descriptionMagnet, True)

# magnetRecord[ 'value' ] = 7730.0
# magnetFolder.storeObject( cool.ValidityKeyMin, cool.ValidityKeyMax, magnetRecord, 1)
# magnetFolder.storeObject( cool.ValidityKeyMin, cool.ValidityKeyMax, magnetRecord, 2)
# magnetRecord[ 'value' ] = 20400.0
# magnetFolder.storeObject( cool.ValidityKeyMin, cool.ValidityKeyMax, magnetRecord, 3)
# magnetFolder.storeObject( cool.ValidityKeyMin, cool.ValidityKeyMax, magnetRecord, 4)

chanstatSpec = cool.RecordSpecification()
chanstatSpec.extend( 'LVCHSTAT_RECV' , cool.StorageType.Int32 )
chanstatSpec.extend( 'STATE'         , cool.StorageType.UInt32 )

chanstatRecord = cool.Record(chanstatSpec)
chanstatRecord[ 'LVCHSTAT_RECV' ] = 209
chanstatRecord[ 'STATE' ] = 17

chanstatFolderSpec = cool.FolderSpecification(chanstatSpec)
chanstatFolder = db.createFolder('/SCT/DCS/CHANSTAT', chanstatFolderSpec, descriptionDCS, True)

for channel in sctChannels:
    chanstatFolder.storeObject( cool.ValidityKeyMin, cool.ValidityKeyMax, chanstatRecord, int(channel) )

hvSpec = cool.RecordSpecification()
hvSpec.extend( 'HVCHVOLT_RECV' , cool.StorageType.Float )
hvSpec.extend( 'HVCHCURR_RECV' , cool.StorageType.Float )

hvRecord = cool.Record(hvSpec)
hvRecord[ 'HVCHVOLT_RECV' ] = 150.0
hvRecord[ 'HVCHCURR_RECV' ] = 10.0

hvFolderSpec = cool.FolderSpecification(hvSpec)
hvFolder = db.createFolder('/SCT/DCS/HV', hvFolderSpec, descriptionDCS, True)

for channel in sctChannels:
    hvFolder.storeObject( cool.ValidityKeyMin, cool.ValidityKeyMax, hvRecord, int(channel) )

modtempSpec = cool.RecordSpecification()
modtempSpec.extend( 'MOCH_TM0_RECV' , cool.StorageType.Float )
modtempSpec.extend( 'MOCH_TM1_RECV' , cool.StorageType.Float )

modtempRecord = cool.Record(modtempSpec)
modtempRecord[ 'MOCH_TM0_RECV' ] = 7.0
modtempRecord[ 'MOCH_TM1_RECV' ] = 7.0

modtempFolderSpec = cool.FolderSpecification(modtempSpec)
modtempFolder = db.createFolder('/SCT/DCS/MODTEMP', modtempFolderSpec, descriptionDCS, True)

for channel in sctChannels:
    modtempFolder.storeObject( cool.ValidityKeyMin, cool.ValidityKeyMax, modtempRecord, int(channel) )

db.closeDatabase()
