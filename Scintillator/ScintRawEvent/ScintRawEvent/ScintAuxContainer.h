#ifndef XAODFASERSCINT_SCINTAUXCONTAINER_H
#define XAODFASERSCINT_SCINTAUXCONTAINER_H
 
// System include(s):
extern "C" {
#   include <stdint.h>
}
#include <vector>

// EDM include(s):
#include "xAODCore/AuxContainerBase.h"
#include "AthLinks/ElementLink.h"

// Local include(s):
#include "xAODFaserScint/Scint.h"
 
namespace xAOD {
    class ScintAuxContainer : public AuxContainerBase {
    
    public:
    /// Default constructor
    ScintAuxContainer();
    /// Dumps contents (for debugging)
    void dump() const;
    
    private:
    
    /// @name Defining parameters (perigee)
    /// @{
    std::vector< float >                x0;
    std::vector< float >                y0;
    std::vector< float >                phi0;
    std::vector< float >                theta;
    std::vector< float >                qOverP;
    
    std::vector< std::vector<float> >   definingParametersCovMatrix;
    
    std::vector< float >                vx;
    std::vector< float >                vy;
    std::vector< float >                vz;
    /// @}
    
    // /// @name Parameters
        /// We store the 3-pos, 3-mom and charge, and on the transient side these will be transformed into curvilinear parameters.
        /// Also stored are the cov matrix (still expressed in local coordinate frame) and parameter position.
        /// @{
        std::vector< std::vector<float> >   parameterX;
        std::vector< std::vector<float> >   parameterY;
        std::vector< std::vector<float> >   parameterZ;
        std::vector< std::vector<float> >   parameterPX;
        std::vector< std::vector<float> >   parameterPY;
        std::vector< std::vector<float> >   parameterPZ;

        std::vector< std::vector<float> >   scintParameterCovarianceMatrices;
        std::vector< std::vector<uint8_t> > parameterPosition;
    
    /* Sam : cluster removal
    std::vector< xAOD::Scint::StripClusterLinks_t > clusterLinks;
    std::vector< uint32_t > hitPattern;
    */
    
    /// @name Fit quality functions
    /// @{
    std::vector< float >                chiSquared;
    std::vector< float >                numberDoF;
    /// @}
    
    /// @name ScintInfo functions
    /// @{
    std::vector< uint8_t >              scintFitter;
    std::vector< uint8_t >              particleHypothesis;
    /// @}
    
    #ifndef XAODFASERSCINT_SUMMARYDYNAMIC
    /// @name ScintSummary information
    /// @{
    std::vector< uint8_t >         numberOfContribPMTLayers       ;
    std::vector< uint8_t >         numberOfPMTHits                  ;
    std::vector< uint8_t >         numberOfPMTOutliers              ;
    std::vector< uint8_t >         numberOfPMTHoles                 ;
    std::vector< uint8_t >         numberOfPMTDoubleHoles           ;
    std::vector< uint8_t >         numberOfPMTSharedHits            ;
    std::vector< uint8_t >         numberOfPMTDeadSensors           ;
    std::vector< uint8_t >         numberOfPMTSpoiltHits            ;
    
    std::vector< uint8_t >         numberOfOutliersOnScint          ;
    std::vector< uint8_t >         standardDeviationOfChi2OS        ;
    
    /// @}
    #endif
    }; // class ScintAuxContainer
}

// Set up a CLID and StoreGate inheritance for the class:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::ScintAuxContainer, 1087894799, 1 )


#endif // XAODFASERSCINT_SCINTAUXCONTAINER_H