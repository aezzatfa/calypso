#ifndef XAODFASERSCINT_PMTRAWDATACONTAINER_H
#define XAODFASERSCINT_PMTRAWDATACONTAINER_H

// Core include(s):
#include "AthContainers/DataVector.h"

// Local include(s):
#include "xAODFaserScint/PMTRawData.h"

namespace xAOD {
   /// The container is a simple typedef for now
   typedef DataVector<xAOD::PMTRawData> PMTRawDataContainer;
}

// Set up a CLID for the container:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF(xAOD::PMTRawDataContainer, 1213701819, 1)

#endif // XAODFASERTRACKING_PMTRAWDATACONTAINER_H