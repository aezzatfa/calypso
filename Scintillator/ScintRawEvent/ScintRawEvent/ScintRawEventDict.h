// Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef XAODFASERSCINT_XAODFASERSCINTDICT_H
#define XAODFASERSCINT_XAODFASERSCINTDICT_H

// Needed to successfully generate the dictionary in standalone mode:
#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#   define EIGEN_DONT_VECTORIZE
#endif // __GCCXML__

// System include(s):
#include <bitset>
 
// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLinkVector.h"
 
// Local include(s):
#include "xAODFaserScint/ScintContainer.h"
#include "xAODFaserScint/ScintAuxContainer.h"

#include "xAODFaserScint/PMTRawDataContainer.h"
#include "xAODFaserScint/PMTRawDataAuxContainer.h"

#include "xAODFaserScint/FaserScintPrimitives.h"

namespace {
  struct GCCXML_DUMMY_INSTANTIATION_XAODFASERSCINT {
      
      xAOD::ScintContainer                                              c1;
      DataLink< xAOD::ScintContainer >                                  l1;
      ElementLink< xAOD::ScintContainer >                               l2;
      ElementLinkVector< xAOD::ScintContainer >                         l3;
      std::vector< DataLink< xAOD::ScintContainer > >                   l4;
      std::vector< ElementLink< xAOD::ScintContainer > >                l5;
      std::vector< ElementLinkVector< xAOD::ScintContainer > >          l6;
      std::vector< std::vector< ElementLink< xAOD::ScintContainer > > > l7;

      // Container(s):
      xAOD::PMTClusterContainer                                                    c4;
      // Data link(s):
      DataLink< xAOD::PMTClusterContainer >                                        pdl1;
      std::vector< DataLink< xAOD::PMTClusterContainer > >                         pdl2;
      // Element link(s):
      ElementLink< xAOD::PMTClusterContainer >                                     pel1;
      std::vector< ElementLink< xAOD::PMTClusterContainer > >                   pel2;
      std::vector< std::vector< ElementLink< xAOD::PMTClusterContainer > > >    pel3;

      // Container(s):
      xAOD::PMTRawDataContainer                                                    c6;
      // Data link(s):
      DataLink< xAOD::PMTRawDataContainer >                                        rdodl1;
      std::vector< DataLink< xAOD::PMTRawDataContainer > >                         rdodl2;
      // Element link(s):
      ElementLink< xAOD::PMTRawDataContainer >                                        rdoel1;
      std::vector< ElementLink< xAOD::PMTRawDataContainer > >                      rdoel2;
      std::vector< std::vector< ElementLink< xAOD::PMTRawDataContainer > > >       rdoel3;

  };
}

#endif // XAODFASERSCINT_XAODFASERSCINTDICT_H