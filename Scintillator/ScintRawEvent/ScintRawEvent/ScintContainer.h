#ifndef XAODFASERSCINT_SCINTCONTAINER_H
#define XAODFASERSCINT_SCINTCONTAINER_H

// Core include(s):
#include "AthContainers/DataVector.h"
#include <stdint.h>
 
// Local include(s):
#include "xAODFaserScint/Scint.h"

//template struct DataVector_detail::DVLEltBaseInit< xAOD::Scint >;
 
namespace xAOD {
   typedef DataVector< xAOD::Scint > ScintContainer;
}

// Set up a CLID for the container:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::ScintContainer, 1313291760, 1 )

#endif // XAODFASERSCINT_SCINTCONTAINER_H