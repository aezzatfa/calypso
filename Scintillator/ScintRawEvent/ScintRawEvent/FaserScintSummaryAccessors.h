// Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: ScintSummaryAccessors_v1.h 574227 2013-12-06 14:20:39Z emoyse $
#ifndef XAOD_FASERSCINTSUMMARYACCESSORS_H
#define XAOD_FASERSCINTSUMMARYACCESSORS_H

// EDM include(s):
#include "AthContainers/AuxElement.h"

// Local include(s):
#include "xAODFaserScint/FaserScintPrimitives.h"

namespace xAOD {

   /// Helper function for managing ScintSummary Accessor objects
   ///
   /// This function holds on to Accessor objects that can be used by each
   /// ScintParticle_v1 object at runtime to get/set summary values on themselves.
   ///
   template <class T>
   SG::AuxElement::Accessor< T >*
   faserScintSummaryAccessor( xAOD::FaserSummaryType type );

} // namespace xAOD

#endif // XAOD_FASERSCINTSUMMARYACCESSORS_H
