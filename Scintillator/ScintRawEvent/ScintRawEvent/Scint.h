#ifndef XAODFASERSCINT_SCINT_H
#define XAODFASERSCINT_SCINT_H

// System include(s):
#include <bitset>
extern "C" {
#   include <stdint.h>
}

// Core include(s):
#include "AthLinks/ElementLink.h"
#include "AthContainers/DataVector.h"

// xAOD include(s):
#include "xAODBase/IParticle.h"
#include "xAODFaserBase/FaserObjectType.h"
#include "xAODFaserScint/FaserScintPrimitives.h"

namespace xAOD {
    
    class Scint : public IParticle {
    
    public:
    
        /// Default constructor
        Scint();
        /// Destructor
        ~Scint();
        /// Copy ctor. This involves copying the entire Auxilary store, and is a slow operation which should be used sparingly.
        Scint(const Scint& o );
        /// Assignment operator. This can involve creating and copying an Auxilary store, and so should be used sparingly.
        Scint& operator=(const Scint& tp );
        
        /// @name IParticle functions
        /// @{
            /// The transverse momentum (\f$p_T\f$) of the particle.
            virtual double           pt() const;
            /// The pseudorapidity (\f$\eta\f$) of the particle.
            virtual double           eta() const;
            /// The azimuthal angle (\f$\phi\f$) of the particle (has range \f$-\pi\f$ to \f$+\pi\f$.)
            virtual double           phi() const;
            /// The invariant mass of the particle..
            virtual double           m() const;
            /// The total energy of the particle.
            virtual double           e() const;
            /// The true rapidity (y) of the particle.
            virtual double           rapidity() const;
    
            /// Definition of the 4-momentum type.
            typedef IParticle::FourMom_t FourMom_t;
    
            /// The full 4-momentum of the particle.
            virtual FourMom_t p4() const;
            
            /// The type for the IParticle, this should not be used
            virtual Type::ObjectType type() const;
    
            /// The type of the object as a simple enumeration
            FaserType::ObjectType faserType() const;
        /// @}
        
        /* Sam : removing cluster stuff
        /// @name ElementLinks to PMTClusters
        /// @{
            typedef std::vector< ElementLink< xAOD::StripClusterContainer > > StripClusterLinks_t;
            const StripClusterLinks_t& clusterLinks() const;
            void setClusterLinks(const StripClusterLinks_t& clusterLinks);
        /// @}
        */
        
        /// @name Defining parameters functions
        /// The 'defining parameters' are key to the concept of a Scint, and give the values for the IParticle interface
        /// ( pt(), phi(), eta() etc.).
        /// They use the Trk::Perigee coordinate system, and are defined as:
        ///  \f$( d_0, z_0, \phi, \theta, q/p )\f$.
        /// The parameters are expressed with respect to an origin (returned by vx(), vy() and vy() ), currently intended to be the 'beamspot'.
        /// This origin is expected to be the same for all Scint particles in a collection (and this may be be enforced).
        /// The \f$\phi\f$ parameter is returned by either the phi() or the phi0() methods, the difference just being whether it is returned as a float or a double (it is stored as a float)
        /// @{
            /// Returns the charge.
            float charge() const;
            /// Returns the \f$x_0\f$ parameter
            float x0() const;
            /// Returns the \f$y_0\f$ parameter
            float y0() const;
            /// Returns the \f$phi_0\f$ parameter
            float phi0() const;
            /// Returns the \f$\theta\f$  parameter, which has range 0 to \f$\pi\f$.
            float theta() const;
            /// Returns the \f$q/p\f$  parameter
            float qOverP() const;
            /// @brief Returns a SVector of the Perigee Scint parameters. 
            /// i.e. a vector of
            ///  \f$\left(\begin{array}{c}x_0\\y_0\\\phi\\\theta\\q/p\end{array}\right)\f$
            const DefiningParameters_t definingParameters() const;
            /// Returns the 5x5 symmetric matrix containing the defining parameters covariance matrix.
            const ParametersCovMatrix_t definingParametersCovMatrix() const;  
            /// Returns the length 6 vector containing the elements of defining parameters covariance matrix.
            const std::vector<float>& definingParametersCovMatrixVec() const;   
            /// Set the defining parameters.     
            void setDefiningParameters(float x0, float y0, float phi0, float theta, float qOverP);
            /// Set the defining parameters covariance matrix.
            void setDefiningParametersCovMatrix(const ParametersCovMatrix_t& cov);
            /// Set the defining parameters covariance matrix using a length 15 vector.
            void setDefiningParametersCovMatrixVec(const std::vector<float>& cov);
            /// The x origin for the parameters.
            float vx() const;
            /// The y origin for the parameters.
            float vy() const;
            /// The z origin for the parameters.
            float vz() const;
            /// Set the origin for the parameters.
            void setParametersOrigin(float x, float y, float z);
   
    
        /// @name Curvilinear functions
        /// The set of functions which return other Scint parameters.
        /// The remaining Scint parameters (i.e. not the 'defining parameters') use the 'curvilinear' coordinate system, 
        /// and are represented by the parameters @f$(x,y,z,p_x,p_y,p_z)@f$.
        /// The parameters can have an associated local 5x5 error/covariance matrix. They are expressed at various points through the
        /// detector, which can be determined by the parameterPosition() method.      
        /// @code
        /// // Example code to use parameters
        /// unsigned int index=0;
        /// if (myTP.indexOfParameterAtPosition(index, xAOD::FirstMeasurement)){
        ///   CurvilinearParameters_t parameters = myTP.scintParameters(index);
        /// }
        /// @endcode
        /// @{
            /// Returns the number of additional parameters stored in the ScintParticle. 
            size_t numberOfParameters() const; 
            /// Returns the Scint parameter vector at 'index'.
            const CurvilinearParameters_t scintParameters(unsigned int index) const;
            /// Returns the parameter x position, for 'index'.
            float parameterX(unsigned int index) const;
            /// Returns the parameter y position, for 'index'.
            float parameterY(unsigned int index) const;
            /// Returns the parameter z position, for 'index'.
            float parameterZ(unsigned int index) const;
            /// Returns the parameter x momentum component, for 'index'.
            float parameterPX(unsigned int index) const;
            /// Returns the parameter y momentum component, for 'index'.
            float parameterPY(unsigned int index) const;
            /// Returns the parameter z momentum component, for 'index'.
            float parameterPZ(unsigned int index) const; 
            /// Set the parameters via the passed vector of vectors. 
            /// The vector<float> should be of size 6: x,y,z,px,py,pz (charge is stored elsewhere)
            void setScintParameters(std::vector<std::vector<float> >& parameters);
            /// @brief Returns the ScintParticleCovMatrix_t (covariance matrix) at 'index', 
            /// which corresponds to the parameters at the same index.
            ParametersCovMatrix_t scintParameterCovarianceMatrix(unsigned int index) const;
            /// Set the cov matrix of the parameter at 'index', using a vector of floats.
            /// The vector @f$\mathrm{v}(a1,a2,a3 ... a_{15})@f$ represents the lower diagonal, i.e. it gives a matrix of 
            /// \f$\left(\begin{array}{ccccc} a_1  & a_2  & a_4  & a_7  & a_{11} \\ a_2  & a_3  & a_5  & a_8  & a_{12} \\ a_4  & a_5  & a_6  & a_9  & a_{13} \\ a_7  & a_8  & a_9  & a_{10}  & a_{14} \\ a_{11} & a_{12} & a_{13} & a_{14} & a_{15} \end{array}\right)\f$
            void setScintParameterCovarianceMatrix(unsigned int index, std::vector<float>& cov);  
            /// @brief Return the FaserParameterPosition of the parameters at 'index'.
            xAOD::FaserParameterPosition parameterPosition(unsigned int index) const;
            /// @brief Function to determine if this ScintParticle contains Scint parameters at a certain position, and if so, what the 'index' is.
            /// @param[in] index Filled with the index of the Scint parameters at 'position' - untouched otherwise.
            /// @param[out] position The location in the detector of the required Scint parameters.
            /// @return Returns 'true' if the ScintParticle parameters at 'position', returns False otherwise.
            bool indexOfParameterAtPosition(unsigned int& index, FaserParameterPosition position) const;
            /// Set the 'position' (i.e. where it is in ATLAS) of the parameter at 'index', using the FaserParameterPosition enum. 
            void setParameterPosition(unsigned int index, FaserParameterPosition pos);

        
        uint32_t hitPattern() const;
        void setHitPattern(uint32_t hitpattern);
    
        /// @}
    
        /// @name Fit quality functions
        /// Returns some information about quality of the Scint fit.
        /// @{
            /// Returns the @f$ \chi^2 @f$ of the overall Scint fit.
            float chiSquared() const;
            /// Returns the number of degrees of freedom of the overall Scint or vertex fit as float.
            float  numberDoF() const;   
            /// Set the 'Fit Quality' information.
            void setFitQuality(float chiSquared, float numberDoF);   
        /// @}
    
        /// @name ScintInfo functions
        /// Contains information about the 'fitter' of this Trk::Scint / ScintParticle.
        /// Additionally there is some information about how the e.g. fit was configured. 
        /// Also the information on the properties of the  Scint fit is stored.
        /// @{
            /// Method for setting the fitter, using the FaserScintFitter enum.
            void setScintFitter(const FaserScintFitter fitter)  ;
            /// Method for setting the particle type, using the FaserParticleHypothesis enum.
            void setParticleHypothesis(const FaserParticleHypothesis hypo);
        /// Returns the particle hypothesis used for Scint fitting.
            FaserParticleHypothesis particleHypothesis() const;
            /// Returns the fitter.
            FaserScintFitter scintFitter() const;
        /// @}
    
    
            /// Accessor for ScintSummary values.
            /// If 'information' is stored in this ScintParticle and is of the correct templated type T, 
            /// then the function fills 'value' and returns 'true', otherwise it returns 'false', and does not touch 'value'. 
            /// See below for an example of how this is intended to be used.
            /// @code
            /// int numberOfBLayerHits=0;
            /// if( myParticle.summaryValue(numberOfBLayerHits,xAOD::numberOfBLayerHits) ){
            ///   ATH_MSG_INFO("Successfully retrieved the integer value, numberOfBLayerHits"); 
            /// }
            /// float numberOfCscPhiHits=0.0; //Wrong! This is actually an int too.
            /// if( !myParticle.summaryValue(numberOfCscPhiHits,xAOD::numberOfCscPhiHits) ){
            ///   ATH_MSG_INFO("Types must match!"); 
            /// }
            /// @endcode
            /// @param[in] information The information being requested. This is not guaranteed to be stored in all ScintParticles.
            /// @param[out] value  Only filled if this ScintParticle contains 'information', and the types match.
            /// @return Returns 'true' if the ScintParticle contains 'information', and its concrete type matches 'value' (templated type T).
            bool summaryValue(uint8_t& value, const FaserSummaryType &information) const;
            ///  @copydoc Scint::summaryValue(uint8_t& value, const FaserSummaryType &information) const
            bool summaryValue(float& value, const FaserSummaryType &information) const;
            /// Set method for ScintSummary values.
            void setSummaryValue(uint8_t& value, const FaserSummaryType &information);
            ///  @copydoc Scint::setSummaryValue(uint8_t& value, const FaserSummaryType &information)
            void setSummaryValue(float& value, const FaserSummaryType &information);
        /// @}

        /// @}
        private:

        }; // class Scint Particle
}

// Set up a CLID for the class:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::Scint, 2550112, 1 )

#endif // XAODFASERSCINT_SCINT_H