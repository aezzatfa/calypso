#ifndef XAODFASERSCINT_PMTRAWDATAAUXCONTAINER_H
#define XAODFASERSCINT_PMTRAWDATAAUXCONTAINER_H

// System include(s):
#include <vector>

// Core include(s):
#include "xAODCore/AuxContainerBase.h"
#include "AthLinks/ElementLink.h"
 
namespace xAOD {
    class PMTRawDataAuxContainer : public AuxContainerBase {
    public:
        PMTRawDataAuxContainer();
        /// Dumps contents (for debugging)
        void dump() const;
    private:
        std::vector<uint64_t> id;
        std::vector<uint32_t> dataword;
    };
}

// Set up a CLID for the container:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::PMTRawDataAuxContainer , 1226475826 , 1 )
 
#endif // XAODFASERSCINT_PMTRAWDATAAUXCONTAINER_H