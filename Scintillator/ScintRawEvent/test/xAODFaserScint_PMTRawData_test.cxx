// System include(s):
#include <iostream>

// Local include(s):
#include "xAODFaserScint/PMTRawDataContainer.h"
#include "xAODFaserScint/PMTRawDataAuxContainer.h"

template< typename T >
std::ostream& operator<< ( std::ostream& out,
                           const std::vector< T >& vec ) {

   out << "[";
   for( size_t i = 0; i < vec.size(); ++i ) {
      out << vec[ i ];
      if( i < vec.size() - 1 ) {
         out << ", ";
      }
   }
   out << "]";
   return out;
}

/// Function filling one PMT RDO with information
void fill( xAOD::PMTRawData& sc ) {

   sc.setWord( (uint32_t) 1010111);
   sc.setId( (uint64_t) 1111111111111);

   return;
}

/// Function printing the properties of a PMTCluster
void print( const xAOD::PMTRawData& sc ) {
    
   std::cout << "id = " << sc.id() << std::endl;
   std::cout << "word = " << sc.getWord() << std::endl;

   return;
}

int main() {

   // Create the main containers to test:
   xAOD::PMTRawDataAuxContainer aux;
   xAOD::PMTRawDataContainer tpc;
   tpc.setStore( &aux );

   // Add one PMT cluster to the container:
   xAOD::PMTRawData* p = new xAOD::PMTRawData();
   tpc.push_back( p );

   // Fill it with information:
   fill( *p );

   // Print the information:
   print( *p );

   // Print the contents of the auxiliary store:
   aux.dump();

   // Return gracefully:
   return 0;
}