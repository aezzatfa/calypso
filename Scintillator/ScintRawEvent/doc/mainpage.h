/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/**
   @mainpage xAODScint package

   Original Authors
   @author Edward Moyse <Edward.Moyse@cern.ch>
   @author Andreas Salzburger <Andreas.Salzburger@cern.ch>
   @author Markus Elsing <Markus.Elsing@cern.ch>
   @author Ruslan Mashinistov <Ruslan.Mashinistov@cern.ch>
   @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   
   Modifying Authors
   @author Samuel Meehan <samuel.meehan@cern.ch>
   @author Ke Li <ke.li@cern.ch>
   @author Abdullah Ezzat <aezzatfa@cern.ch>
   

   $Revision: 588420 $
   $Date: 2014-03-19 15:15:25 +0100 (Wed, 19 Mar 2014) $

   @section xAODScintOverview Overview

   To be completed

   @section xAODScintClasses Classes

   To be completed

   @htmlinclude used_packages.html

   @include requirements
*/
