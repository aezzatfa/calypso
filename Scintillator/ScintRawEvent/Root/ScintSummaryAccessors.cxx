// System include(s):
extern "C" {
#   include <stdint.h>
}
#include <iostream>

// Local include(s):
#include "xAODFaserScint/FaserScintSummaryAccessors.h"

/// Helper macro for Accessor objects
#define DEFINE_ACCESSOR(TYPE, NAME )                               \
   case xAOD::NAME:                                                \
   {                                                               \
      static SG::AuxElement::Accessor< TYPE > a( #NAME );          \
      return &a;                                                   \
   }                                                               \
   break;

namespace xAOD {

  // Generic case. Maybe return warning?
  template<class T>
   SG::AuxElement::Accessor< T >*
   faserScintSummaryAccessor( xAOD::FaserSummaryType /*type*/ )
   {}

  template<>
   SG::AuxElement::Accessor< uint8_t >*
   faserScintSummaryAccessor<uint8_t>( xAOD::FaserSummaryType type ) {

      switch( type ) {
        DEFINE_ACCESSOR( uint8_t, numberOfContribPMTLayers        );
        DEFINE_ACCESSOR( uint8_t, numberOfPMTHits                   );
        DEFINE_ACCESSOR( uint8_t, numberOfPMTOutliers               );
        DEFINE_ACCESSOR( uint8_t, numberOfPMTHoles                  );
        DEFINE_ACCESSOR( uint8_t, numberOfPMTDoubleHoles            );
        DEFINE_ACCESSOR( uint8_t, numberOfPMTSharedHits             );
        DEFINE_ACCESSOR( uint8_t, numberOfPMTDeadSensors            );
        DEFINE_ACCESSOR( uint8_t, numberOfPMTSpoiltHits             );
     
        DEFINE_ACCESSOR( uint8_t, numberOfOutliersOnScint           );
        DEFINE_ACCESSOR( uint8_t, standardDeviationOfChi2OS         );
      default:                  
         std::cerr << "xAOD::Scint ERROR Unknown FaserSummaryType ("
                   << type << ") requested" << std::endl;
         return 0;
      }
   } 
} // namespace xAOD
