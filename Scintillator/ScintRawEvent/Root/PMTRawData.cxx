#include "xAODFaserScint/PMTRawData.h"

// EDM include(s):
#include "xAODCore/AuxStoreAccessorMacros.h"

namespace xAOD {
  PMTRawData::PMTRawData() { }

  AUXSTORE_PRIMITIVE_SETTER_AND_GETTER(PMTRawData, uint64_t, id, setId)

  static const SG::AuxElement::Accessor<uint32_t> word_acc("dataword");
  void PMTRawData::setWord(uint32_t new_word) {
    word_acc(*this) = new_word;
  }

  uint32_t PMTRawData::getWord() const {
    return word_acc(*this);
  }

  // decode size of group of PMTs information 
  int PMTRawData::getGroupSize() const {
    return getWord() & 0x7FF;
  }

  // decode PMT information
  int PMTRawData::getPMT() const {
    return (getWord() >> 11) & 0x7FF;
  }

  // decode time bin information
  int PMTRawData::getTimeBin() const {
    return (getWord() >> 22) & 0x7;
  }

  // returns a word incoding the errors
  int PMTRawData::getErrors() const {
    return (getWord() >> 25) & 0x7;
  }

  // returns true if the time bin corresponding to the present BC is on
  bool PMTRawData::OnTime() const {
    return (getWord() >> 23) & 0x1;
  }

  // returns true if there is an error in the first hit's data
  bool PMTRawData::FirstHitError() const {
    return (getWord() >> 29) & 0x1;
  }

  // returns true if there is an error in the second hit's data
  bool PMTRawData::SecondHitError() const {
    return (getWord() >> 30) & 0x1;
  }
} // namespace xAOD