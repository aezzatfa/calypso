// Local include(s):
#include "xAODFaserScint/ScintAuxContainer.h"

namespace xAOD {

   ScintAuxContainer::ScintAuxContainer()
      : AuxContainerBase() {

      AUX_VARIABLE( x0 );
      AUX_VARIABLE( y0 );
      AUX_VARIABLE( phi0 );
      AUX_VARIABLE( theta );
      AUX_VARIABLE( qOverP );

      AUX_VARIABLE( definingParametersCovMatrix );

      AUX_VARIABLE( vx );
      AUX_VARIABLE( vy );
      AUX_VARIABLE( vz );
      
      /* Sam : these seem to be related to the clusters, which we don't want - I think
      AUX_VARIABLE( clusterLinks);
      AUX_VARIABLE( hitPattern );
      */
     
      AUX_VARIABLE( chiSquared          );
      AUX_VARIABLE( numberDoF );

      AUX_VARIABLE( scintFitter          );
      AUX_VARIABLE( particleHypothesis );

      // ScintSummary information
#ifndef XAODSCINT_SUMMARYDYNAMIC
      // uint8_ts
      AUX_VARIABLE( numberOfContribPMTLayers        );
      AUX_VARIABLE( numberOfPMTHits                   );
      AUX_VARIABLE( numberOfPMTOutliers               );
      AUX_VARIABLE( numberOfPMTHoles                  );
      AUX_VARIABLE( numberOfPMTDoubleHoles            );
      AUX_VARIABLE( numberOfPMTSharedHits             );
      AUX_VARIABLE( numberOfPMTDeadSensors            );
      AUX_VARIABLE( numberOfPMTSpoiltHits             );

      AUX_VARIABLE( numberOfOutliersOnScint           );
      AUX_VARIABLE( standardDeviationOfChi2OS         );
#endif

   }

   void ScintAuxContainer::dump() const {
     std::cout<<" Dumping ScintAuxContainer"<<std::endl;
     std::cout<<"x0:";
     std::copy(x0.begin(), x0.end(),
       std::ostream_iterator<float>(std::cout, ", "));
     std::cout<<"y0:";
     std::copy(y0.begin(), y0.end(),
       std::ostream_iterator<float>(std::cout, ", "));
     std::cout<<"phi0:";
     std::copy(phi0.begin(), phi0.end(),
       std::ostream_iterator<float>(std::cout, ", "));
     std::cout<<"theta:";
     std::copy(theta.begin(), theta.end(),
       std::ostream_iterator<float>(std::cout, ", "));
     std::cout<<"qOverP:";
     std::copy(qOverP.begin(), qOverP.end(),
       std::ostream_iterator<float>(std::cout, ", "));
     std::cout<<"definingParametersCovMatrix: ["<<&definingParametersCovMatrix<<"]";
     for (unsigned int i=0; i<definingParametersCovMatrix.size();++i){
     std::copy(definingParametersCovMatrix[i].begin(), definingParametersCovMatrix[i].end(),
       std::ostream_iterator<float>(std::cout, ", "));
        std::cout<<std::endl;
     }
   }

} // namespace xAOD
