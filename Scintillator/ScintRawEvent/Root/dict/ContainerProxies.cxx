// EDM include(s):
#include "xAODCore/AddDVProxy.h"

// Local include(s):
#include "xAODFaserScint/ScintContainer.h"
#include "xAODFaserScint/PMTRawDataContainer.h"

// Set up the collection proxies:
ADD_NS_DV_PROXY( xAOD, ScintContainer );
ADD_NS_DV_PROXY( xAOD, PMTRawDataContainer );