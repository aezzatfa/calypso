// Misc includes
#include <bitset>
#include <vector>

// EDM include(s):
#include "xAODCore/AuxStoreAccessorMacros.h"

// Local include(s):
#include "xAODFaserScint/Scint.h"
#include "xAODFaserScint/FaserScintSummaryAccessors.h"

namespace xAOD {

  Scint::Scint()
  : IParticle() {
      
  }
  
  Scint::Scint(const Scint& tp ) 
  : IParticle( tp ) {
    makePrivateStore( tp );
  }
  
  Scint& Scint::operator=(const Scint& tp ){
    if(this == &tp) return *this;
    
    if( ( ! hasStore() ) && ( ! container() ) ) {
       makePrivateStore();
    }
    this->IParticle::operator=( tp );
    return *this;
  }
  
  Scint::~Scint(){
  }
  
  double Scint::pt() const {
  return p4().Pt();
  }
  
  double Scint::eta() const {
  return p4().Eta(); 
  }
  
  double Scint::phi() const {
  return p4().Phi(); 
  }
  
  double Scint::m() const {
  return p4().M();
  }
  
  double Scint::e() const {
  return p4().E(); 
  }
  double Scint::rapidity() const {
  return p4().Rapidity();
  }

  Scint::FourMom_t Scint::p4() const {
    Scint::FourMom_t p4;
    using namespace std;
    float p = 10.e6; // 10 TeV (default value for very high pt muons, with qOverP==0)
    if (fabs(qOverP())>0.) p = 1/fabs(qOverP());
    float thetaT = theta();
    float phiT = phi();
    float sinTheta= sin(thetaT);
    float px = p*sinTheta*cos(phiT);
    float py = p*sinTheta*sin(phiT);
    float pz = p*cos(thetaT);
    float e  =  pow (139.570,2) + pow( px,2) + pow( py,2) + pow( pz,2);
    p4.SetPxPyPzE( px, py, pz, sqrt(e) ); 
    return p4;
  }

  Type::ObjectType Scint::type() const {
     return Type::Other;
  }
  
  FaserType::ObjectType Scint::faserType() const {
     return FaserType::Scint;
  }

  float Scint::charge() const {
    return (qOverP() > 0) ? 1 : ((qOverP() < 0) ? -1 : 0);
  }

  AUXSTORE_PRIMITIVE_GETTER(Scint, float, x0)
  AUXSTORE_PRIMITIVE_GETTER(Scint, float, y0)
  AUXSTORE_PRIMITIVE_GETTER(Scint, float, phi0)
  AUXSTORE_PRIMITIVE_GETTER(Scint, float, theta)
  AUXSTORE_PRIMITIVE_GETTER(Scint, float, qOverP)

  const DefiningParameters_t Scint::definingParameters() const{
    DefiningParameters_t tmp;
    tmp << x0() , y0() , phi0() , theta() , qOverP();
    return tmp;
  }

  void Scint::setDefiningParameters(float x0, float y0, float phi0, float theta, float qOverP) {
    static Accessor< float > acc1( "x0" );
    acc1( *this ) = x0;

    static Accessor< float > acc2( "y0" );
    acc2( *this ) = y0;

    static Accessor< float > acc3( "phi0" );
    acc3( *this ) = phi0;

    static Accessor< float > acc4( "theta" );
    acc4( *this ) = theta;

    static Accessor< float > acc5( "qOverP" );
    acc5( *this ) = qOverP;

    return;
  }

  void Scint::setDefiningParametersCovMatrix(const xAOD::ParametersCovMatrix_t& cov){

    static Accessor< std::vector<float> > acc( "definingParametersCovMatrix" );
    FMath::compress(cov,acc(*this));
  }

  const xAOD::ParametersCovMatrix_t Scint::definingParametersCovMatrix() const {
    xAOD::ParametersCovMatrix_t cov; 
    const std::vector<float>& covVec = definingParametersCovMatrixVec();
    if( !covVec.empty() ) FMath::expand( covVec.begin(), covVec.end(),cov );
    else cov.setIdentity();
    return cov;
  }

  const std::vector<float>& Scint::definingParametersCovMatrixVec() const {
  // Can't use AUXSTORE_PRIMITIVE_SETTER_AND_GETTER since I have to add Vec to the end of setDefiningParametersCovMatrix to avoid clash.
    static Accessor< std::vector<float> > acc( "definingParametersCovMatrix" );
    return acc(*this);
  }

  void Scint::setDefiningParametersCovMatrixVec(const std::vector<float>& cov){
  // Can't use AUXSTORE_PRIMITIVE_SETTER_AND_GETTER since I have to add Vec to the end of setDefiningParametersCovMatrix to avoid clash.
    static Accessor< std::vector<float> > acc( "definingParametersCovMatrix" );
    acc(*this)=cov;
  }

  AUXSTORE_PRIMITIVE_GETTER(Scint, float, vx)
  AUXSTORE_PRIMITIVE_GETTER(Scint, float, vy)
  AUXSTORE_PRIMITIVE_GETTER(Scint, float, vz)

  void Scint::setParametersOrigin(float x, float y, float z){
    static Accessor< float > acc1( "vx" );
    acc1( *this ) = x;

    static Accessor< float > acc2( "vy" );
    acc2( *this ) = y;

    static Accessor< float > acc3( "vz" );
    acc3( *this ) = z;
  }

  AUXSTORE_PRIMITIVE_GETTER(Scint, float, chiSquared)
  AUXSTORE_PRIMITIVE_GETTER(Scint, float, numberDoF)

  void Scint::setFitQuality(float chiSquared, float numberDoF){
    static Accessor< float > acc1( "chiSquared" );
    acc1( *this ) = chiSquared;  
    static Accessor< float > acc2( "numberDoF" );
    acc2( *this ) = numberDoF;   
  }

/* Sam : this maybe should be removed because it has to do with clusters, which we don't have for scintillators
  static SG::AuxElement::Accessor< Scint::StripClusterLinks_t > clusterAcc( "clusterLinks" );
  AUXSTORE_OBJECT_SETTER_AND_GETTER( Scint, Scint::StripClusterLinks_t, clusterLinks, setClusterLinks )
  AUXSTORE_PRIMITIVE_SETTER_AND_GETTER(Scint, uint32_t, hitPattern, setHitPattern)
*/

  size_t Scint::numberOfParameters() const{
    ///@todo - Can we do this in a better way? Not great to force retrieval of one specific parameter - any would do.
    static Accessor< std::vector<float>  > acc( "parameterX" );
    if(! acc.isAvailable( *this )) return 0;
    return acc(*this).size();
  }

  const CurvilinearParameters_t Scint::ScintParameters(unsigned int index) const{
    CurvilinearParameters_t tmp;
    tmp << parameterX(index),parameterY(index),parameterZ(index),
      parameterPX(index),parameterPY(index),parameterPZ(index);
    return tmp;
  }

  void Scint::setScintParameters(std::vector<std::vector<float> >& parameters){
    static Accessor< std::vector < float > > acc1( "parameterX" );
    static Accessor< std::vector < float > > acc2( "parameterY" );
    static Accessor< std::vector < float > > acc3( "parameterZ" );
    static Accessor< std::vector < float > > acc4( "parameterPX" );
    static Accessor< std::vector < float > > acc5( "parameterPY" );
    static Accessor< std::vector < float > > acc6( "parameterPZ" );

    static Accessor< std::vector<uint8_t>  > acc7( "parameterPosition" );

    acc1(*this).resize(parameters.size());
    acc2(*this).resize(parameters.size());
    acc3(*this).resize(parameters.size());
    acc4(*this).resize(parameters.size());
    acc5(*this).resize(parameters.size());
    acc6(*this).resize(parameters.size());

    acc7(*this).resize(parameters.size());

    unsigned int index=0;
    // std::cout<<"Adding this many parameters: "<<parameters.size()<<std::endl;
    std::vector<std::vector<float> >::const_iterator it=parameters.begin(), itEnd=parameters.end();
    for (;it!=itEnd;++it,++index){
      assert((*it).size()==6);
      acc1(*this).at(index)=(*it).at(0);
      acc2(*this).at(index)=(*it).at(1);
      acc3(*this).at(index)=(*it).at(2);
      acc4(*this).at(index)=(*it).at(3);
      acc5(*this).at(index)=(*it).at(4);
      acc6(*this).at(index)=(*it).at(5);
      // std::cout<<"param=("<<(*it).at(0)<<", "<<(*it).at(0)<<", "<<(*it).at(1)<<", "<<(*it).at(2)<<", "<<(*it).at(3)<<", "<<(*it).at(4)<<", "<<(*it).at(5)<<")"<<std::endl;
    }
  }

  float Scint::parameterX(unsigned int index) const  {
    static Accessor< std::vector<float>  > acc( "parameterX" );
    return acc(*this).at(index);
  }

  float Scint::parameterY(unsigned int index) const  {
    static Accessor< std::vector<float>  > acc( "parameterY" );
    return acc(*this).at(index);
  }

  float Scint::parameterZ(unsigned int index) const  {
    static Accessor< std::vector<float>  > acc( "parameterZ" );
    return acc(*this).at(index);
  }

  float Scint::parameterPX(unsigned int index) const {
    static Accessor< std::vector<float>  > acc( "parameterPX" );
    return acc(*this).at(index);
  }

  float Scint::parameterPY(unsigned int index) const {
    static Accessor< std::vector<float>  > acc( "parameterPY" );
    return acc(*this).at(index);
  }

  float Scint::parameterPZ(unsigned int index) const {
    static Accessor< std::vector<float>  > acc( "parameterPZ" );    
    return acc(*this).at(index);
  }

  xAOD::ParametersCovMatrix_t Scint::scintParameterCovarianceMatrix(unsigned int index) const
  {
    static Accessor< std::vector<float>  > acc( "scintParameterCovarianceMatrices" );
    unsigned int offset = index*15;
    // copy the correct values into the temp matrix
    xAOD::ParametersCovMatrix_t tmp;
    std::vector<float>::const_iterator it = acc(*this).begin()+offset;
    FMath::expand(it,it+15,tmp);
    return tmp;
  }

  void Scint::setScintParameterCovarianceMatrix(unsigned int index, std::vector<float>& cov){
    assert(cov.size()==15);
    unsigned int offset = index*15;
    static Accessor< std::vector < float > > acc( "scintParameterCovarianceMatrices" );
    std::vector<float>& v = acc(*this);
    v.resize(offset+15);
    std::copy(cov.begin(),cov.end(),v.begin()+offset );
  }

  xAOD::FaserParameterPosition Scint::parameterPosition(unsigned int index) const
  {
    static Accessor< std::vector<uint8_t>  > acc( "parameterPosition" );
    return static_cast<xAOD::FaserParameterPosition>(acc(*this).at(index));
  }

  bool Scint::indexOfParameterAtPosition(unsigned int& index, FaserParameterPosition position) const 
  {
    size_t maxParameters = numberOfParameters();
    bool foundParameters=false;
    for (size_t i=0; i<maxParameters; ++i){
      if (parameterPosition(i)==position){
        foundParameters=true;
        index=i;
        break;
      }
    }
    return foundParameters;
  }

  void  Scint::setParameterPosition(unsigned int index, xAOD::FaserParameterPosition pos){
    static Accessor< std::vector<uint8_t>  > acc( "parameterPosition" );
    acc( *this ).at(index) = static_cast<uint8_t>(pos);
  }

  AUXSTORE_PRIMITIVE_GETTER_WITH_CAST(Scint, uint8_t, xAOD::FaserScintFitter,scintFitter)
  AUXSTORE_PRIMITIVE_SETTER_WITH_CAST(Scint, uint8_t, xAOD::FaserScintFitter,scintFitter, setScintFitter)

  AUXSTORE_PRIMITIVE_SETTER_WITH_CAST(Scint, uint8_t, xAOD::FaserParticleHypothesis, particleHypothesis, setParticleHypothesis)
  AUXSTORE_PRIMITIVE_GETTER_WITH_CAST(Scint, uint8_t, xAOD::FaserParticleHypothesis, particleHypothesis)

  bool Scint::summaryValue(uint8_t& value, const FaserSummaryType &information)  const {
    xAOD::Scint::Accessor< uint8_t >* acc = faserScintSummaryAccessor<uint8_t>( information );
    if( ( ! acc ) || ( ! acc->isAvailable( *this ) ) ) return false;
  // Retrieve the value:
    value = ( *acc )( *this );
    return true;
  }
  
  void Scint::setSummaryValue(uint8_t& value, const FaserSummaryType &information){
    xAOD::Scint::Accessor< uint8_t >* acc = faserScintSummaryAccessor<uint8_t>( information );
  // Set the value:
    ( *acc )( *this ) = value;
  }

} // namespace xAOD