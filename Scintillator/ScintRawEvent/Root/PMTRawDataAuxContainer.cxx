#include "xAODFaserScint/PMTRawDataAuxContainer.h"

namespace xAOD {
  PMTRawDataAuxContainer::PMTRawDataAuxContainer()
    : AuxContainerBase()
  {
    AUX_VARIABLE(id);
    AUX_VARIABLE(dataword);
  }
  
    void PMTRawDataAuxContainer::dump() const {
    std::cout<<" Dumping PMTRawDataAuxContainer"<<std::endl;
    std::cout<<"id:";
    std::copy(id.begin(), id.end(),
        std::ostream_iterator<float>(std::cout, ", "));
    std::cout<<"dataword:";
    std::copy(dataword.begin(), dataword.end(),
        std::ostream_iterator<float>(std::cout, ", "));
        std::cout<<std::endl;
  }
}