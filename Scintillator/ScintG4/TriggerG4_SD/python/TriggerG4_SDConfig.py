# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

from AthenaCommon import CfgMgr
from G4AtlasApps.SimFlags import simFlags

from ISF_Algorithms.collection_merger_helpers import generate_mergeable_collection_name


def getTriggerSensorSD(name="TriggerSensorSD", **kwargs):
    bare_collection_name = "TriggerHits"
    mergeable_collection_suffix = "_G4"
    merger_input_property = "TriggerHits"
    hits_collection_name = generate_mergeable_collection_name(bare_collection_name,
                                                              mergeable_collection_suffix,
                                                              merger_input_property)
    kwargs.setdefault("LogicalVolumeNames", ["Trigger::Plate"])
    kwargs.setdefault("OutputCollectionNames", [hits_collection_name])
    return CfgMgr.TriggerSensorSDTool(name, **kwargs)

