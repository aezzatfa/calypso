# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

from AthenaCommon import CfgMgr

def getTriggerDetectorTool(name="TriggerDetectorTool", **kwargs):
    kwargs.setdefault("DetectorName",     "Trigger");
    kwargs.setdefault("Alignable",        True);
    kwargs.setdefault("RDBAccessSvc",     "RDBAccessSvc");
    kwargs.setdefault("GeometryDBSvc",    "ScintGeometryDBSvc");
    kwargs.setdefault("GeoDbTagSvc",      "GeoDbTagSvc");
    return CfgMgr.TriggerDetectorTool(name, **kwargs)

from IOVDbSvc.IOVDbSvcConfig import addFoldersSplitOnline

def TriggerGeometryCfg( flags ):
    from FaserGeoModel.GeoModelConfig import GeoModelCfg
    acc = GeoModelCfg( flags )
    geoModelSvc = acc.getPrimary()

    from GeometryDBSvc.GeometryDBSvcConf import GeometryDBSvc
    acc.addService(GeometryDBSvc("ScintGeometryDBSvc"))

    from RDBAccessSvc.RDBAccessSvcConf import RDBAccessSvc
    acc.addService(RDBAccessSvc("RDBAccessSvc"))

    from DBReplicaSvc.DBReplicaSvcConf import DBReplicaSvc
    acc.addService(DBReplicaSvc("DBReplicaSvc"))

    from TriggerGeoModel.TriggerGeoModelConf import TriggerDetectorTool
    triggerDetectorTool = TriggerDetectorTool()

    triggerDetectorTool.useDynamicAlignFolders = flags.GeoModel.Align.Dynamic
    geoModelSvc.DetectorTools += [ triggerDetectorTool ]

    # if flags.GeoModel.Align.Dynamic:
    #     acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/AlignL1/ID","/Indet/AlignL1/ID",className="CondAttrListCollection"))
    #     acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/AlignL2/SCT","/Indet/AlignL2/SCT",className="CondAttrListCollection"))
    #     acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/AlignL3","/Indet/AlignL3",className="AlignableTransformContainer"))
    # else:
    #     if (not flags.Detector.SimulateTrigger) or flags.Detector.OverlayTrigger:
    #         acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/Align","/Indet/Align",className="AlignableTransformContainer"))
    #     else:
    #         acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/Align","/Indet/Align"))

    # if flags.Common.Project is not "AthSimulation": # Protection for AthSimulation builds
    #     if (not flags.Detector.SimulateTrigger) or flags.Detector.OverlayTrigger:
    #         from TriggerConditionsAlgorithms.TriggerConditionsAlgorithmsConf import TriggerAlignCondAlg
    #         triggerAlignCondAlg = TriggerAlignCondAlg(name = "TriggerAlignCondAlg",
    #                                             UseDynamicAlignFolders = flags.GeoModel.Align.Dynamic)
    #         acc.addCondAlgo(triggerAlignCondAlg)
    #         from TriggerConditionsAlgorithms.TriggerConditionsAlgorithmsConf import TriggerDetectorElementCondAlg
    #         triggerDetectorElementCondAlg = TriggerDetectorElementCondAlg(name = "TriggerDetectorElementCondAlg")
    #         acc.addCondAlgo(triggerDetectorElementCondAlg)

    return acc
