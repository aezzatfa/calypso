// System include(s):
#include <exception>

// Local include(s):
#include "xAODStripClusterAuxContainerCnv.h"
#include "AthContainers/tools/copyThinned.h"
#include "AthenaKernel/IThinningSvc.h"

xAODStripClusterAuxContainerCnv::
xAODStripClusterAuxContainerCnv( ISvcLocator* svcLoc )
   : xAODStripClusterAuxContainerCnvBase( svcLoc ) {

}

xAOD::StripClusterAuxContainer*
xAODStripClusterAuxContainerCnv::
createPersistent( xAOD::StripClusterAuxContainer* trans ) {

   // Create a copy of the container:
   return SG::copyThinned (*trans, IThinningSvc::instance());
}

xAOD::StripClusterAuxContainer*
xAODStripClusterAuxContainerCnv::createTransient() {

   // The known ID(s) for this container:
   static const pool::Guid v1_guid( "61B62A1A-4C51-43A2-8364-1B9E910A81E8" );

   // Check which version of the container we're reading:
   if( compareClassGuid( v1_guid ) ) {
      // It's the latest version, read it directly:
      return poolReadObject< xAOD::StripClusterAuxContainer >();
   }

   // If we didn't recognise the ID:
   throw std::runtime_error( "Unsupported version of "
                             "xAOD::StripClusterAuxContainer found" );
   return 0;
}