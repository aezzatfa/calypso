#ifndef XAODFASERTRUTH_TRUTHEVENTAUXCONTAINER_H
#define XAODFASERTRUTH_TRUTHEVENTAUXCONTAINER_H

// System include(s):
#include <vector>

// EDM include(s):
#include "AthLinks/ElementLink.h"
#include "xAODCore/AuxContainerBase.h"

// Local include(s):
#include "xAODFaserTruth/FaserTruthParticleContainer.h"
#include "xAODFaserTruth/FaserTruthVertexContainer.h"

namespace xAOD {

   /// Auxiliary store for the truth vertices
   ///
   class FaserTruthEventAuxContainer : public AuxContainerBase {

   public:
      /// Default constructor
      FaserTruthEventAuxContainer();

   private:
      /// @name Links to the interaction particles/vertices
      /// @{
      std::vector< ElementLink< FaserTruthVertexContainer > >
      signalProcessVertexLink;
      /// @}

      /// @todo Needs to be a map or similar (perhaps two linked vectors?)
      std::vector< std::vector< float > > weights;

      /// @name Cross sections and errors coming from the generator
      /// @{
      std::vector< float > crossSection;
      std::vector< float > crossSectionError;
      /// @}

      /// @name Links to the generated/simulated particles/vertices
      /// @{
      std::vector< std::vector< ElementLink< FaserTruthParticleContainer > > >
      truthParticleLinks;
      std::vector< std::vector< ElementLink< FaserTruthVertexContainer > > >
      truthVertexLinks;
      /// @}
      
      //Two vectors (of vectors) to store association between weight name and weight
      //index. No std::map is used for increased read-back speed in ROOT
      std::vector < std::vector < std::string > > weightNames;
      std::vector < uint32_t > mcChannelNumber;

   }; // class FaserTruthEventAuxContainer

} // namespace xAOD

// Declare a CLID for the class
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::FaserTruthEventAuxContainer, 1254985314, 1 )

#endif // XAODFASERTRUTH_TRUTHEVENTAUXCONTAINER_H
