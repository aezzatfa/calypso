atlas_subdir( SimHitExample )

atlas_depends_on_subdirs( PRIVATE
                    Generators/GeneratorObjects
                    Control/AthenaBaseComps
                    Tracker/TrackerSimEvent
        )

atlas_add_component( SimHitExample
                    src/SimHitAlg.cxx
                    src/components/SimHitExample_entries.cxx
                    LINK_LIBRARIES AthenaBaseComps GeneratorObjects TrackerSimEvent
        )

atlas_install_joboptions( share/*.py )