/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "GeoAdaptors/GeoScintHit.h"
#include "GeoAdaptors/GeoFaserSiHit.h"

const ScintDD::VetoDetectorManager      *GeoScintHit::s_veto = 0;
const ScintDD::TriggerDetectorManager   *GeoScintHit::s_trigger = 0;
const ScintDD::PreshowerDetectorManager *GeoScintHit::s_preshower = 0;
const TrackerDD::SCT_DetectorManager    *GeoFaserSiHit::s_sct;
const VetoID                            *GeoScintHit::s_vID = 0;
const TriggerID                         *GeoScintHit::s_tID = 0;
const PreshowerID                       *GeoScintHit::s_pID = 0;
const FaserSCT_ID                       *GeoFaserSiHit::s_sID = 0;

