################################################################################
# Package: GeoModelFaserUtilities
################################################################################
#
# Only one class in the ATLAS GeoModelUtilities library needs to be modified for FASER
# so instead of duplicating the entire library, create a small one with only
# the modified class
#

# Declare the package name:
atlas_subdir( GeoModelFaserUtilities )

# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          Control/AthenaBaseComps
                          Control/AthenaKernel
                          Database/RDBAccessSvc
                          DetectorDescription/GeoModel/GeoModelInterfaces
                          PRIVATE
                          GaudiKernel )

# External dependencies:
find_package( Boost COMPONENTS filesystem thread system )
find_package( Eigen )
find_package( CORAL COMPONENTS CoralBase CoralKernel RelationalAccess )
find_package( GeoModelCore )

# Component(s) in the package:
atlas_add_library( GeoModelFaserUtilities
                   src/*.cxx
                   PUBLIC_HEADERS GeoModelFaserUtilities
                   INCLUDE_DIRS ${Boost_INCLUDE_DIRS} ${CORAL_INCLUDE_DIRS} ${EIGEN_INCLUDE_DIRS} ${GEOMODEL_INCLUDE_DIRS}
                   LINK_LIBRARIES ${Boost_LIBRARIES} ${CORAL_LIBRARIES} ${EIGEN_LIBRARIES} ${GEOMODEL_LIBRARIES} AthenaBaseComps SGTools 
                   PRIVATE_LINK_LIBRARIES GaudiKernel )

