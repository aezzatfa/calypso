###############################
# Package: FaserGeoModel
###############################

# Declare package name
atlas_subdir( FaserGeoModel )

add_custom_command (
   OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/geomDB_sqlite
   DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/data/geomDB.sql
   COMMAND cat ${CMAKE_CURRENT_SOURCE_DIR}/data/geomDB.sql | sqlite3 ${CMAKE_CURRENT_BINARY_DIR}/geomDB_sqlite
   )

add_custom_target( geomDB ALL DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/geomDB_sqlite )

# Install the generated file:
install( FILES ${CMAKE_CURRENT_BINARY_DIR}/geomDB_sqlite
         DESTINATION ${CMAKE_INSTALL_DATADIR} )

# Install python files from the package:
atlas_install_python_modules( python/*.py )

# Install xml files from the package:
atlas_install_xmls( data/*.xml data/*.dtd )

atlas_install_scripts( test/*.py )

atlas_add_test( EVNT_InputGeo_test
                SCRIPT test/FaserGeometryConfig_EVNT_test.py
                PROPERTIES TIMEOUT 300 
                PROPERTIES WORKING_DIRECTORY ${CMAKE_BINARY_DIR})
